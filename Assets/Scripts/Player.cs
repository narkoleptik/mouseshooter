﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameObject _projectile;
    [SerializeField]
    private GameObject _target;

    private float _speed = 15.0f;

    // Start is called before the first frame update
    void Start()
    {
        _target = GameObject.Find("Target").GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.Space)) || (Input.GetMouseButtonDown(0)))
        {
            if (_target == null)
            {
                Debug.Log("Target object not found");
            }
            Vector3 aim = _target.transform.position;

            Vector3 difference = (aim - transform.position).normalized;
            float distance = difference.magnitude;
            Vector2 direction = difference / distance;
            direction.Normalize();
            float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            GameObject projectile = Instantiate(_projectile, transform.position, Quaternion.identity);
            projectile.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);
            projectile.GetComponent<Rigidbody>().velocity = direction * _speed;
            Debug.Log("Shooting something - direction: " + direction + " mypos: " + transform.position + " target: " + _target.transform.position + " direction: " + direction);

        }
    }
}
