﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //[SerializeField] GameObject _projectile;

    //private float _speed = 50.0f;

    // Start is called before the first frame update
    void Start()
    {
        /*
            Vector3 shootDirection;
            shootDirection = Input.mousePosition;
            shootDirection.z = 0.0f;
            shootDirection = Camera.main.ScreenToWorldPoint(shootDirection);
            shootDirection = shootDirection-transform.position;
            //...instantiating the rocket
            Rigidbody2D bulletInstance = Instantiate(rocket, transform.position, Quaternion.Euler(new Vector3(0,0,0))) as Rigidbody2D;
            bulletInstance.velocity = new Vector2(shootDirection.x * speed, shootDirection.y * speed);
        */

        /*
        Vector3 mousepos = Input.mousePosition;
        mousepos.z = 0;
        //Vector 3 mousepos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Debug.Log("Mouse Position: " + mousepos);
        //Vector3 lookdirection = (mousepos - transform.position).normalized;
        Vector2 direction = (Vector2)((mousepos - transform.position)).normalized;
        Debug.Log("Mouse Position: " + mousepos + " Direction: " + direction);
        Rigidbody rb = GetComponent<Rigidbody>();
        //rb.AddForce(direction * _speed);
        rb.velocity = new Vector3(direction.x * _speed, direction.y * _speed, 0);
        */
    }

    // Update is called once per frame
    void Update()
    {
        if ((transform.position.x > 20.0f) || (transform.position.y > 20.0f))
        {
            Destroy(this.gameObject);
        }
    }
}
